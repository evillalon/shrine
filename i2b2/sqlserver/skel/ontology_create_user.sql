---modified to fit SQL Server 2014-09-06
---create shrine_ont database with sql 
CREATE DATABASE  $(I2B2_DB_SHRINE_ONT_USER)  COLLATE SQL_Latin1_General_CP1_CI_AS;
GO

---create shrine_ont login for shrine_ont database 
USE $(I2B2_DB_SHRINE_ONT_USER)
CREATE LOGIN $(I2B2_DB_SHRINE_ONT_USER) WITH PASSWORD= '$(I2B2_DB_SHRINE_ONT_PASSWORD)', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF,DEFAULT_DATABASE=$(I2B2_DB_SHRINE_ONT_USER);
GO

---grant permissions to a ROLE ReadExecute
CREATE ROLE ReadExecute AUTHORIZATION dbo;
GO
USE $(I2B2_DB_SHRINE_ONT_USER)
GRANT ALTER, EXECUTE, DELETE, SELECT, INSERT, UPDATE, CREATE SEQUENCE ON SCHEMA::[dbo] TO [ReadExecute]
GO

---create user shrine_ont for login shrine_ont
USE $(I2B2_DB_SHRINE_ONT_USER)
CREATE USER $(I2B2_DB_SHRINE_ONT_USER) FOR LOGIN $(I2B2_DB_SHRINE_ONT_USER)  WITH DEFAULT_SCHEMA=dbo;
GO

---grant permissions to login i2b2_shrine_ont_user for database shrine_ont
USE $(I2B2_DB_SHRINE_ONT_USER)
--EXEC sp_addrolemember @rolename='db_ddladmin', @rolename=@DB_USER@;
EXEC  sp_addrolemember 'ReadExecute',$(I2B2_DB_SHRINE_ONT_USER);
EXEC sp_addrolemember  @rolename=[db_datareader],   @membername=$(I2B2_DB_SHRINE_ONT_USER) ;
EXEC sp_addrolemember @rolename=[db_datawriter], @membername=$(I2B2_DB_SHRINE_ONT_USER);
EXEC sp_addrolemember @rolename=[db_owner], @membername=$(I2B2_DB_SHRINE_ONT_USER);
EXEC sp_addrolemember @rolename=[db_accessadmin],  @membername=$(I2B2_DB_SHRINE_ONT_USER) ;
GO

---add i2b2metadata user to shrine_ont database and grant permissions
USE $(I2B2_DB_SHRINE_ONT_USER)
CREATE USER $(I2B2_DB_ONT_USER) FOR LOGIN $(I2B2_DB_ONT_USER)
GO

---grant permissions
USE $(I2B2_DB_SHRINE_ONT_USER)
EXEC sp_addrolemember @rolename='ReadExecute',@membername=$(I2B2_DB_ONT_USER);
EXEC sp_addrolemember @rolename=[db_datareader],@membername=$(I2B2_DB_ONT_USER);
EXEC sp_addrolemember @rolename=[db_datawriter], @membername=$(I2B2_DB_ONT_USER);
EXEC sp_addrolemember @rolename=[db_owner], @membername=$(I2B2_DB_ONT_USER);
EXEC sp_addrolemember @rolename=[db_accessadmin],  @membername=$(I2B2_DB_ONT_USER) ;
GO



