-- modified to fit SQL Server 2014-09-06
---create shrine_ont database with sql 
CREATE DATABASE  $(I2B2_DB_USER)  COLLATE SQL_Latin1_General_CP1_CI_AS;
GO

---create shrine_ont login for shrine_ont database 
USE $(I2B2_DB_USER)
CREATE LOGIN $(I2B2_DB_USER) WITH PASSWORD= '$(I2B2_DB_PASSWORD)', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF,DEFAULT_DATABASE=$(I2B2_DB_USER);
GO


---create user shrine_ont for login shrine_ont
USE $(I2B2_DB_USER)
CREATE USER $(I2B2_DB_USER) FOR LOGIN $(I2B2_DB_USER)  WITH DEFAULT_SCHEMA=dbo;
GO
---grant permissions to login i2b2_shrine_ont_user for database shrine_ont
USE $(I2B2_DB_USER)
EXEC  sp_addrolemember @rolename=[db_datareader],   @membername=$(I2B2_DB_USER) ;
EXEC  sp_addrolemember @rolename=[db_datawriter],  @membername=$(I2B2_DB_USER) ;
EXEC  sp_addrolemember @rolename=[db_owner],   @membername=$(I2B2_DB_USER) ;
GO


