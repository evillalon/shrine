-- The ONT and CRC DB lookup tables provide path information for the ont-ds.xml
delete from i2b2hive.dbo.ONT_DB_LOOKUP where C_PROJECT_PATH = 'SHRINE/';
GO
delete from i2b2hive.dbo.CRC_DB_LOOKUP where C_PROJECT_PATH = '/SHRINE/';
GO