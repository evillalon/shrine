### SHRINE INSTALLATION FOR WINDOWS
===============================

###Automates installation of Shrine on Windows
============================================

####Authors: Elena Villalon

####Contact: https://www.dbmi.columbia.edu/ 

####emails: ev2348@columbia.edu/ evillalon@post.harvard.edu

#### Columbia University Department of Biomedical Informatics


The original bash scripts for the implementation of Shrine are translated to equivalent Ant scripts. 

Requirements:You need to download [Ant] (http://ant.apache.org/bindownload.cgi) and put it in your path so you can execute Ant commands.  
 
**There are 5 folders (besides the docs folder) in the Shrine installation folder:**
 
1. **shrine/** contains ant file to install Shrine server, a Tomcat application 

2. **i2b2/** with ant files to modify i2b2 so that it can support the Shrine application.

3. **lib/** contains some jars files that are needed to run svn with Ant for folder *shrine/*, and the svnant.zip.  

4. **files-tlp/** spare folder that contains some js, xml, and sql files that duplicate some of the files in the installation folders **shrine/** and **i2b2/** for reasons of maintaining clean copies. 
 Ant scripts replace "@@token@" in files templates of sub-folders of  **shrine/skel/** and **i2b2/sqlserver/skel/** with values stored in the *.properties* files.  

5. **svn-shrine-download/** contains a single file that downloads files from given url such as shrine-webclient from url-shrine

It also contains a *common.properties* file that needs to be edited according to values specific to the installation of Shrine and i2b2. 
  
  The file names and their contents in the folders above follow very closely the original implementation See [Shrine website] (https://open.med.harvard.edu/wiki/display/SHRINE/Manual+Server+Installation)
  The names of executable files correspond to the names of the original executables bash (.sh) files with extensions that ends in'.xml'. 
  The properyty files *common.properties*, *shrine.properties*, *i2b2.properties* contain parameters that need to be edited for each installation and are the equivalent to  *common.rc*, *shrine.rc* and *i2b2.rc* in the bash scripts. 
 
##Pre-requisites to install Shrine Server:

For the installation of the Shrine Server, you will need to have MySQL installed. The Ant script that downloads MySQL is *shrine/install_prereqs.xml* and the target *get-mysql-installer* for the windows automated installer that you will need to run manually and which will also make mysql a Service. Otherwise the target *main* will get the zip file and unzip it.

##Install Shrine Server:

You will need to edit the parameters in *common.properties* and *shrine.properties* depending on the configuration of your shrine server and the location of the i2b2 services, in the example is localhost:9090. For more information read the instructions in the original website above for the configuration files common.rc and shrine.rc.  

After you have mysql installed, then you go into the folder *shrine/* and then run the Ant file with the command: *ant -f install.xml*. This command will execute the default targets for files mysql.xml, install-tomcat.xml, keystore.xml, shrine-conf.xml, tomcat.xml and will install shrine server and tomcat in the folder of your choice.

   
##Pre-requisites to prepare I2B2 for the shrine installation:

I2B2 and Shrine do not necessarily need to be in the same PC. The Shrine IP address is obtained in the shrine server during the installation and needs to be a numeric IP address. Also the parameter *SHRINE_SERVER_IP* in *i2b2.properties* is the Shrine IP address, which, if it is the same machine as Shrine Server, then it will be calculated with the scripts or, otherwise, you can enter the IP address of Shrine Server.   

To configure I2B2 to work with Shrine obviously you need an instance of i2b2 installed.  However for testing the scripts you may just configure part of the i2b2 installation. Specifically,
 
1. you need to download JBOSS (i.e. jboss-as-7.1.1.Final).

2. you also need to install the express version of SQL Server and the Management Studio.

3. create the databases and user that supports i2b2. In the folder i2b2/ execute the Ant script: *ant -f  create_db_users.xml*, which will create databases, logins and users for the cells PM, HIVE, CRC, IM, ONTOLOGY, WORK.  

4. populate the databases with the tables and data of i2b2demo. 
For this purpose go to [I2B2 website] (https://www.i2b2.org/software/index.html), Software section and download i2b2-createdb-17.02.zip, and with the i2b2 documentation and Ant scripts in the zip file you may install the tables, triggers and Harvard demodata.  

This minimum requirements will be sufficient to run the scripts for the i2b2 part of the Shrine installation.  

##Install Shrine Server:

To modify i2b2 to work with Shrine go to folder *i2b2/* specified the *i2b2.properties* for your domain as explained for the file *i2b2.rc* in the original website [Shrine website] (https://open.med.harvard.edu/wiki/display/SHRINE/Manual+Server+Installation). Then, run the script: *ant -f prepare.xml* that will execute all the scripts needed for i2b2 configuration, i.e. configure_hive.xml, configure_pm.xml and ontology.xml.  For a brand new installation if one existed earlier run first: *ant -f clean.xml* follow by *ant -f prepare.xml* 
 
##Support for I2B2 Server

Because it is an internal Server the files cannot be downloaded from Harvard websites and need to be stored in specific folders as detailed next.  
The scripts in *shrine/* folder check if those files are stored in sub-folder *work/*: Those files are create_broadcaster_audit_table.sql, adapter.sql, shrine.war, shrine-proxy.war, shrine-webclient.zip and AdapterMappings.xml.
If they are in *shrine/work/* the server will not attempt to download them and use the ones in the sub-folder */work/*. 

Same way, the *i2b2/* folder contains sub-folder *backup/*. If file AdapterMappings.xml is in the *i2b2/backup/* it will not attempt to download it.  

##Instalattion of Shrine Steward

Configure the parameters relevant to Stweard app in *shrine.properties*, use the file *steward-only-ant.sql* to create the Stweard MySQl database/user and then proceed to run the ant script *ant -f  install-steward-ant.xml*

